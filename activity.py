while True:
    year = input("Enter a year: ")
    if not year.isdigit() or int(year) <= 0:
        print("Please enter a positive integer.")
    else:
        year = int(year)
        break

if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
    print(f"{year} is a leap year.")
else:
    print(f"{year} is not a leap year.")

while True:
    row = input("Enter the number of rows: ")
    col = input("Enter the number of columns: ")
    if not row.isdigit() or not col.isdigit() or int(row) <= 0 or int(col) <= 0:
        print("Please enter positive integers for both row and column.")
    else:
        row = int(row)
        col = int(col)
        break

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()